﻿//
// Адаптер к метаданным. Реализует интерфейс
// 
// 
// boolean АдаптерМетаданныеЭто()
// 
// boolean ИспользованиеВозможно()
// 
// boolean Инициализировать()
// 
// int Приоритет()
// 
// string УстройствоКод(КомпьютерИмя)
// 
// string УстройствоИмя(КомпьютерИмя)
// 
// random ХранилищеНастройкаУмолчание(Ключ)
// 

// Возвращает принадлежность обработки к адаптерам к метаданным логирования
//
// Параметры
//
// Возвращаемое значение:
//   Булево
//
Функция АдаптерМетаданныеЭто() Экспорт
	
	Возврат Истина;
	
КонецФункции //АдаптерМетаданныеЭто 

// Возвращает флаг возможности использования
//
// Параметры
//
// Возвращаемое значение:
//   Булево
//
Функция ИспользованиеВозможно() Экспорт
	
	Возврат Метаданные.ПланыОбмена.Найти("Мобильные") <> Неопределено;
	
КонецФункции //ИспользованиеВозможно 

// Возвращает флаг успеха инициализации
//
// Параметры
//
// Возвращаемое значение:
//   Булево
//
Функция Инициализировать() Экспорт
	
	// В случае использовния адаптера в комфигурации
	// клиента мобильных бригад для записи лога используем 
	// удаленный сервер, иначе (на сервере обмена) используем
	// текущий хост
	
	Результат	= Истина;
	
	// Если это клиент мобильных бригад и есть
	// адрес сервера обмена, то пытаемся получить настройки 
	// логирования с сервера обмена
	Если он_Строка.ПрефиксЕсть(Метаданные.Имя, "МобильныеБригады")
		// И он_Препроцессор.ЭтоМобильныйСервер()
		И Метаданные.Константы.Найти("АдресЦентральнойБазы") <> Неопределено
		Тогда
		
		СерверАдрес	= Константы.АдресЦентральнойБазы.Получить();
		
		Если НЕ ПустаяСтрока(СерверАдрес) Тогда
			
			Определение	= он_ИнтернетСервер.ВебСервисОпределение(СерверАдрес
			, л4с_Система.ВебСервисПубликацияФайлИмя());
			
			Если Определение <> Неопределено Тогда
				
				Прокси	= он_ИнтернетСервер.ВебСервис(Определение
				, л4с_Система.ВебСервисURIПространствоИмен()
				, л4с_Система.ВебСервисИмя());
				
				Если Прокси <> Неопределено Тогда
					
					// Используем локальные методы получения кодаи имени
					// текущего устройства поскольку иначе будет циклическая
					// попытка инициализации адаптера
					УстройствоКод	= УстройствоКод(Неопределено);
					УстройствоИмя	= УстройствоИмя(Неопределено);
					Настройка		= л4с_НастройкиСервер.Получить(УстройствоКод);
					
					Настройка.Включено	= Прокси.Включено(УстройствоКод);
					Настройка.Уровень	= л4с_ЛогСервер.УровеньСсылка(Прокси.Уровень(УстройствоКод));
					Настройка.Хранилище	= Прокси.Хранилище(УстройствоКод);
					
					л4с_НастройкиСервер.Установить(УстройствоКод, Настройка);
				КонецЕсли;
			КонецЕсли;
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат Истина;
	
КонецФункции //Инициализировать 

// Возвращает приоритет использования адаптера
//
// Параметры
//
// Возвращаемое значение:
//   Число
//
Функция Приоритет() Экспорт
	
	Возврат 2;
	
КонецФункции //Приоритет 

// Возвращает код текущего устройства
//
// Параметры
//
// Возвращаемое значение:
//   Строка
//
Функция УстройствоКод(КомпьютерИмя) Экспорт
	
	Возврат ПланыОбмена.Мобильные.ЭтотУзел().Код;
	
КонецФункции //УстройствоКод 

// Возвращает имя текущего устройства
//
// Параметры
// 	КомпьютерИмя
//
// Возвращаемое значение:
//   Строка
//
Функция УстройствоИмя(КомпьютерИмя) Экспорт
	
	Возврат ПланыОбмена.Мобильные.ЭтотУзел().Наименование;
	
КонецФункции //УстройствоИмя 

// Возвращает значение по-умолчанию настройки хранилища лога
//
// Параметры
// 	Ключ
//
// Возвращаемое значение:
//   Произвольный
//
Функция ХранилищеНастройкаУмолчание() Экспорт
	
	Возврат Неопределено;
	
КонецФункции //ХранилищеНастройкаУмолчание 
